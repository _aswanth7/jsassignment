
const getCarInfoByID = function (inventory,id){
    if(inventory== null || inventory.length == 0 || id == null ) return [];
    for(let i =0;i<inventory.length;i++){
        if(id === inventory[i].id)
            return inventory[i];
    }
    return [];
};

module.exports = getCarInfoByID;


