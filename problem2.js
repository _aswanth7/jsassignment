const inventory = require("./data");
const getCarInfoByID = require("./problem1");

const getLastCarInfo = function (inventory){
    if (inventory == null || inventory.length == 0) return [];
    let lastCar = inventory[inventory.length - 1];
    return lastCar; 
};

module.exports = getLastCarInfo;