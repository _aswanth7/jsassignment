const getSortedCarModels = function (inventory){
    let sortedCarModels= [];
    if (inventory == null || inventory.length == 0) return [];
    for(let i =0;i<inventory.length;i++){
        sortedCarModels.push(inventory[i].car_model.toUpperCase());
    }
   return sortedCarModels.sort();

};

module.exports = getSortedCarModels;