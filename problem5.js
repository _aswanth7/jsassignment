const getCarYear = require('./problem4')

const getOlderCars = function (inventory,year){
    let olderCars= [];
    if (inventory == null || inventory.length == 0 || year == null) return [];
    let carYear = getCarYear(inventory);
    for(let i =0;i<carYear.length;i++){
        if(carYear[i] < year) 
            olderCars.push(inventory[i]);
    }
    return olderCars;
};

module.exports = getOlderCars;


