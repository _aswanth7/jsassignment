const getFilteredCars = function (inventory,cars){
    let filteredCars= [];
    if (inventory == null || inventory.length == 0 || cars == null || cars.length == 0 ) return [];
    for(let i =0;i<cars.length;i++){
        for(let j =0;j<inventory.length;j++){
            if(cars[i] === inventory[j].car_make) 
            filteredCars.push(inventory[j]);
        }
    }
    
    return filteredCars;
};

module.exports = getFilteredCars;