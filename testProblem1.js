const inventory = require('./data');
const getCarInfoByID = require('./problem1');

let car = getCarInfoByID(inventory,33);
car.length == 0 ? console.log(car) : console.log(`Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`);