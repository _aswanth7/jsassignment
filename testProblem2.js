const inventory = require('./data');
const getLastCarInfo = require('./problem2')

let lastCar = getLastCarInfo(inventory);
if(lastCar.length == 0) console.log(lastCar);
else console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);